# HTML and CSS Training Materials

## Description

In this repo is a basic project with information and application of HTML and CSS.  You will learn what HTML and CSS are, what both can do and how you can use each language.

---

## To Get Started

1. Open a terminal
2. Run the command: `mkdir ~/projects && cd ~/projects`
3. Run the command: `git clone git@bitbucket.org:trevorglick/training-html-css.git` *You may have to enter in your bitbucket key password*
4. Run the command: `cd ~/projects/training-html-css`

Now you should have the project files on your machine. Awwww yeah, we did it. 🚀

---

## Presentation

This project is supposed to be accompanied by someone guiding you along.  Hopefully with Powerpoint slides.

---

## Helpful Links

- [Mozilla Developer Network: HTML](https://developer.mozilla.org/en-US/docs/Web/css)
- [Mozilla Developer Network: HTML](https://developer.mozilla.org/en-US/docs/Web/html)
- [w3.org: HTML](https://www.w3.org/html/)
- [w3.org: CSS](https://www.w3.org/Style/css/)

---

## Glossary

- HTML: HyperText Markup Language is the most basic building block of the Web. It describes and defines the content of a webpage along with the basic layout of the webpage. [source](https://developer.mozilla.org/en-US/docs/Web/html)
- CSS: a stylesheet language used to describe the presentation of a document. CSS describes how elements should be rendered on screen. [source](https://developer.mozilla.org/en-US/docs/Web/css)
- Tag: In HTML a tag is used for creating an element.  The name of an HTML element is the name used in angle brackets such as `<p>` for paragraph.  Note that the end tag's name is preceded by a slash character, "`</p>`", and that in empty elements the end tag is neither required nor allowed. If attributes are not mentioned, default values are used in each case. [source](https://developer.mozilla.org/en-US/docs/Glossary/Tag)
- Element: An element is a part of a webpage. An element may contain a data item or a chunk of text or an image, or perhaps nothing. A typical element includes an opening tag with some attributes, enclosed text content, and a closing tag. [source](https://developer.mozilla.org/en-US/docs/Glossary/Element)
- Div: The generic container for flow content. It has no effect on the content or layout until styled using CSS. As a "pure" container, the `<div>` element does not inherently represent anything. [source](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/div)
- Class: is a space-separated list of the classes of the element. Classes allows CSS and Javascript to select and access specific elements via the class selectors or functions like the DOM method document.getElementsByClassName. [source](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/class)
